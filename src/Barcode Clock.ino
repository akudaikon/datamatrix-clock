/*
 * ----------------------------------------------------------------------------------
 *  ESP-12E Pinout:
 * ----------------------------------------------------------------------------------
 *                   ------------------------
 *                 --| Reset       D1 (TX0) |-- Serial TX/Prog RX
 *       LDR (10K) --| ADC         D3 (RX0) |-- Serial RX/Prog TX
 *             VCC --| CHPD        D4 (SCL) |-- RTC/HT16K33 SCL (10k PU)
 *                 --| D16         D5 (SDA) |-- RTC/HT16K33 SDA (10k PU)
 *                 --| D14 (SCK)         D0 |-- Bootloader (low - program, high - normal)
 *                 --| D12 (MISO)  D2 (TX1) |-- RTC Interrupt B (1Hz)
 * RTC Interrupt A --| D13 (MOSI)  D15 (SS) |-- GND (for normal startup)
 *                 --| VCC              GND |--
 *                   ------------------------
 * ----------------------------------------------------------------------------------
 *  ESP-12E: Generic ESP8266 Module, 160MHz, 4M (1M SPIFFS)
 *
 *  LED Matrices PCB Layout
 *    -------------
 *    | 0 | 4 | 6 |
 *    -------------
 *    | 1 | 5 | 7 |
 *    -------------
 *    | 2 | 3 | 8 |
 *    -------------
 */

// TODO: Work on light ADC range, up to 100K?
// TODO: Display auto off if below dark threshold
// TODO: Check if already in sleep time during startup/settings change
// TODO: Have web UI show sleep state

#include <Arduino.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include "ESP8266HTTPUpdateServerSPIFFS.h"
#include <DNSServer.h>
#include <WiFiManager.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <Ticker.h>
#include <DS1337RTC.h>
#include <TimeLib.h>
#include <Timezone.h>
#include <EEPROM.h>
#include <FS.h>

#define MATRIXROWS              24
#define MATRIXCOLS              24
#define DATACWCOUNT             36
#define ERRORCWCOUNT            24
#define TOTALCWCOUNT            DATACWCOUNT + ERRORCWCOUNT

//#define LDR_SAMPLES             5     // Number of light samples to keep in rolling average
#define LDR_CONSTRAIN           200   // LDR ADC reading lower bound
#define LIGHT_FILTER_PERCENT    0.25

#define RTC_INTA_PIN            13
#define RTC_INTB_PIN            2
#define NTP_POOL                "us.pool.ntp.org"

#define HT16K33_START_ADDR      0x70
#define HT16K33_OSC_ON          0x21
#define HT16K33_OSC_OFF         0x20
#define HT16K33_DISPLAY_ON      0x81
#define HT16K33_DISPLAY_OFF     0x80
#define HT16K33_BRIGHTNESS      0xE0
#define BLINK_INTERVAL          1000  // 1 sec

enum EEPROMSettings
{
  SETTING_INITIALIZED,
  SETTING_AUTOBRIGHT,
  SETTING_BRIGHTNESS,
  SETTING_SLEEPENABLE,
  SETTING_SLEEPSTARTHR,
  SETTING_SLEEPSTARTMIN,
  SETTING_SLEEPENDHR,
  SETTING_SLEEPENDMIN,
  SETTING_24HOUR,
  SETTING_TIMEZONESTD,
  SETTING_TIMEZONEDST,
  NUM_OF_SETTINGS
};

//uint16_t lightSamples[LDR_SAMPLES] = { 0 };
//uint16_t lightTotal = 0;
uint16_t lightValue = 0;
uint16_t lastLightValue = 0;
uint8_t brightness = 15;
bool blinkState = false;

MDNSResponder mdns;
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

TimeChangeRule timeDST = {"DST", Second, Sun, Mar, 2, 0};
TimeChangeRule timeSTD = {"STD", First, Sun, Nov, 2, 0};
Timezone myTZ(timeDST, timeSTD);
TimeChangeRule *tcr;

Ticker ntpUpdate_tckr, displayBlink_tckr;

bool autoBrightness = true;
bool sleepEnable = false;
bool sleepMode = false;
bool twentyFourHourTime = false;

bool updateDisplayFlag = false;
bool updateBrightnessFlag = false;
bool updateTimeFlag = false;

// Polynomial coefficient table for 24x24
const unsigned char coeffTable[] = { 0, 26, 82, 5, 100, 146, 204, 62, 131, 12, 242, 146, 176,
                                     131, 75, 39, 211, 128, 126, 57, 70, 127, 153, 195 };

// Log table for Galois field
const int logTab[] = {  -255, 255, 1, 240, 2, 225, 241, 53, 3,
                        38, 226, 133, 242, 43, 54, 210, 4, 195, 39, 114, 227, 106, 134, 28,
                        243, 140, 44, 23, 55, 118, 211, 234, 5, 219, 196, 96, 40, 222, 115,
                        103, 228, 78, 107, 125, 135, 8, 29, 162, 244, 186, 141, 180, 45, 99,
                        24, 49, 56, 13, 119, 153, 212, 199, 235, 91, 6, 76, 220, 217, 197,
                        11, 97, 184, 41, 36, 223, 253, 116, 138, 104, 193, 229, 86, 79, 171,
                        108, 165, 126, 145, 136, 34, 9, 74, 30, 32, 163, 84, 245, 173, 187,
                        204, 142, 81, 181, 190, 46, 88, 100, 159, 25, 231, 50, 207, 57, 147,
                        14, 67, 120, 128, 154, 248, 213, 167, 200, 63, 236, 110, 92, 176, 7,
                        161, 77, 124, 221, 102, 218, 95, 198, 90, 12, 152, 98, 48, 185, 179,
                        42, 209, 37, 132, 224, 52, 254, 239, 117, 233, 139, 22, 105, 27, 194,
                        113, 230, 206, 87, 158, 80, 189, 172, 203, 109, 175, 166, 62, 127,
                        247, 146, 66, 137, 192, 35, 252, 10, 183, 75, 216, 31, 83, 33, 73,
                        164, 144, 85, 170, 246, 65, 174, 61, 188, 202, 205, 157, 143, 169, 82,
                        72, 182, 215, 191, 251, 47, 178, 89, 151, 101, 94, 160, 123, 26, 112,
                        232, 21, 51, 238, 208, 131, 58, 69, 148, 18, 15, 16, 68, 17, 121, 149,
                        129, 19, 155, 59, 249, 70, 214, 250, 168, 71, 201, 156, 64, 60, 237,
                        130, 111, 20, 93, 122, 177, 150 };

// Antilog table for Galois field
const int aLogTab[] = { 1, 2, 4, 8, 16, 32, 64, 128, 45, 90,
                        180, 69, 138, 57, 114, 228, 229, 231, 227, 235, 251, 219, 155, 27, 54,
                        108, 216, 157, 23, 46, 92, 184, 93, 186, 89, 178, 73, 146, 9, 18, 36,
                        72, 144, 13, 26, 52, 104, 208, 141, 55, 110, 220, 149, 7, 14, 28, 56,
                        112, 224, 237, 247, 195, 171, 123, 246, 193, 175, 115, 230, 225, 239,
                        243, 203, 187, 91, 182, 65, 130, 41, 82, 164, 101, 202, 185, 95, 190,
                        81, 162, 105, 210, 137, 63, 126, 252, 213, 135, 35, 70, 140, 53, 106,
                        212, 133, 39, 78, 156, 21, 42, 84, 168, 125, 250, 217, 159, 19, 38, 76,
                        152, 29, 58, 116, 232, 253, 215, 131, 43, 86, 172, 117, 234, 249, 223,
                        147, 11, 22, 44, 88, 176, 77, 154, 25, 50, 100, 200, 189, 87, 174, 113,
                        226, 233, 255, 211, 139, 59, 118, 236, 245, 199, 163, 107, 214, 129,
                        47, 94, 188, 85, 170, 121, 242, 201, 191, 83, 166, 97, 194, 169, 127,
                        254, 209, 143, 51, 102, 204, 181, 71, 142, 49, 98, 196, 165, 103, 206,
                        177, 79, 158, 17, 34, 68, 136, 61, 122, 244, 197, 167, 99, 198, 161,
                        111, 222, 145, 15, 30, 60, 120, 240, 205, 183, 67, 134, 33, 66, 132,
                        37, 74, 148, 5, 10, 20, 40, 80, 160, 109, 218, 153, 31, 62, 124, 248,
                        221, 151, 3, 6, 12, 24, 48, 96, 192, 173, 119, 238, 241, 207, 179, 75,
                        150, 1 };

void setUpdateDisplayFlag() { updateDisplayFlag = true; }
void setUpdateTimeFlag() { updateTimeFlag = true; }

void setup()
{
  Serial.begin(115200);

  // Initialize EEPROM
  EEPROM.begin(NUM_OF_SETTINGS);
  readSettings();

  // Initialize SPIFFS
  SPIFFS.begin();

  // Initialize LED display
  Wire.begin();
  Wire.setClock(400000);
  initDisplay();

  // Set LED brightness
  if (autoBrightness) sampleLight();
  //if (autoBrightness) for (uint8_t i = 0; i < LDR_SAMPLES; i++) sampleLight();  // fill up light sample array
  setDisplayBrightness(brightness);

  // Connect to Wifi
  WiFiManager wifiManager;
  wifiManager.setAPCallback(wifiConfigModeCallback);
  displayDataMatrix(F("Connecting to wifi..."));
  if (!wifiManager.autoConnect("BarcodeClock"))
  {
    displayDataMatrix(F("Wifi failed! Reset power and retry"));
    blinkDisplay(true);
    while(1) yield();
  }
  blinkDisplay(false);

  // Initialize RTC
  setSyncProvider(RTC.sync);
  RTC.clearAlarmFlag(RTC_ALARM1);
  RTC.clearAlarmFlag(RTC_ALARM2);
  RTC.setSquareWaveRate(RTC_RATE_1HZ);

  // Sync time with NTP
  // If fails, try again in 5 sec
  displayDataMatrix(F("Syncing time with NTP..."));
  while (!syncTime()) delay(5000);

  // Start mDNS
  mdns.begin("barcodeclock");

  // Configure and start HTTP server
  server.on("/getsettings", getSettings);
  server.on("/savesettings", saveSettings);
  server.on("/resetwifi", [&wifiManager]()
  {
    server.send(200, "text/plain", "Clearing saved wifi settings and restarting...");
    wifiManager.resetSettings();
    ESP.restart();
  });
  server.on("/restart", []()
  {
    server.send(200, "text/plain", "Restarting...");
    ESP.restart();
  });
  server.onNotFound([]()
  {
    if (!handleHTTPFileRead(server.uri())) server.send(404, "text/plain", "File Not Found");
  });
  httpUpdater.setup(&server);
  httpUpdater.setStartCallback(httpUpdateStart);
  httpUpdater.setFailCallback(httpUpdateFail);
  server.begin();

  // Enable alarm interrupt from RTC (INTA/INTB active low)
  attachInterrupt(RTC_INTA_PIN, sleepCheck, FALLING);

  // Enable seconds interrupt from RTC
  attachInterrupt(RTC_INTB_PIN, setUpdateDisplayFlag, RISING);
}

void loop()
{
  server.handleClient();
  if (updateDisplayFlag) { displayTime(); updateDisplayFlag = false; }
  if (updateTimeFlag)
  {
    if (syncTime()) ntpUpdate_tckr.once(86400, setUpdateTimeFlag);  // If sync successful, queue another in 24 hours
    else ntpUpdate_tckr.once(300, setUpdateTimeFlag);               // If sync failed, try again in 5 minutes
    updateTimeFlag = false;
  }
  yield();
}

// Encodes ASCII into data codewords
int encodeDataCodeWords(String text, unsigned char dataCodeWords[])
{
  int numCW = 0;

  for (int i = 0; i < text.length(); i++)
  {
    unsigned char c = text[i];

    // Compress two consecutive digits ((1st * 10) + 2nd + 130)
    if ((c >= 48 && c <=57) && (text[i+1] >= 48 && text[i+1] <= 57))
    {
      c = (((c - 48) * 10) + (unsigned char)(text[i+1] - 48)) + 130;
      i++;
    }
    // Otherwise ascii code + 1
    else
    {
      c++;
    }

    dataCodeWords[numCW] = c;
    numCW++;
  }

  return numCW;
}

// Adds padding for any unused data codewords
void padDataCodeWords(int numDataCWs, unsigned char dataCodeWords[])
{
  if (numDataCWs >= DATACWCOUNT) return; // No padding needed

  dataCodeWords[numDataCWs] = 129; // End of message indicator

  // Add padding CWs
  for (int i = numDataCWs + 1; i < DATACWCOUNT; i++)
  {
    int r = ((149 * (i + 1)) % 253) + 1;
    dataCodeWords[i] = (129 + r) % 254;
  }
}

int galoisMult(int a, int b)
{
  if(!a || !b) return 0;
  return aLogTab[(logTab[a] + logTab[b]) % 255];
}

int galoisDoub(int a, int b)
{
  if (!a) return 0;
  if (!b) return a;
  return aLogTab[(logTab[a] + b) % 255];
}

int galoisSum(int a, int b)
{
  return a ^ b;
}

/* Not needed if data matrix stays 24x24. Replaced with coeffTable[]
 * If size changes, replace coeffTable[] values with new ones generated by this func
void calcSolFactorTable(unsigned char table[])
{
  for (int i = 1; i <= ERRORCWCOUNT; i++)
  {
    for(int j = i - 1; j >= 0; j--)
    {
      table[j] = galoisDoub(table[j], i);
      if(j > 0) table[j] = galoisSum(table[j], table[j - 1]);
    }
  }
}
*/

// Calculate and add error correction codewords
void addErrorCorrectCW(unsigned char dataCodeWords[])
{
  int temp = 0;
  int j = 0;
  int i = 0;
  int correctionCW[ERRORCWCOUNT] = { 0 };

  for (i = 0; i < DATACWCOUNT; i++)
  {
    temp = galoisSum(dataCodeWords[i], correctionCW[ERRORCWCOUNT - 1]);

    for (j = ERRORCWCOUNT - 1; j >= 0; j--)
    {
      if (!temp)
        correctionCW[j] = 0;
      else
        correctionCW[j] = galoisMult(temp, coeffTable[j]);

      if (j > 0) correctionCW[j] = galoisSum(correctionCW[j - 1], correctionCW[j]);
    }
  }

  j = DATACWCOUNT;
  for (i = ERRORCWCOUNT - 1; i >= 0; i--)
  {
    dataCodeWords[j] = correctionCW[i];
    j++;
  }
}

// Add finder and timing patterns to barcode
void addFinderPattern(unsigned char dataMatrix[])
{
  for (int y = 0; y < MATRIXCOLS; y++) setDataMatrixBit(0, y, dataMatrix);
  for (int x = 0; x < MATRIXROWS; x++) setDataMatrixBit(x, MATRIXCOLS - 1, dataMatrix);

  for (int y = 1; y < MATRIXCOLS; y+=2) setDataMatrixBit(MATRIXROWS - 1, y, dataMatrix);
  for (int x = 2; x < MATRIXROWS; x+=2) setDataMatrixBit(x, 0, dataMatrix);

  setDataMatrixBit(MATRIXROWS - 3, MATRIXCOLS - 3, dataMatrix);
  setDataMatrixBit(MATRIXROWS - 2, MATRIXCOLS - 2, dataMatrix);
}

// Add a standard shaped codeword block to barcode at given x, y
void addStandardCWBlock(uint8_t x, uint8_t y, unsigned char dataCodeWord, unsigned char dataMatrix[])
{
  if (dataCodeWord & 0x80) setDataMatrixBit(x, y, dataMatrix);
  if (dataCodeWord & 0x40) setDataMatrixBit(x + 1, y, dataMatrix);
  if (dataCodeWord & 0x20) setDataMatrixBit(x, y + 1, dataMatrix);
  if (dataCodeWord & 0x10) setDataMatrixBit(x + 1, y + 1, dataMatrix);
  if (dataCodeWord & 0x08) setDataMatrixBit(x + 2, y + 1, dataMatrix);
  if (dataCodeWord & 0x04) setDataMatrixBit(x, y + 2, dataMatrix);
  if (dataCodeWord & 0x02) setDataMatrixBit(x + 1, y + 2, dataMatrix);
  if (dataCodeWord & 0x01) setDataMatrixBit(x + 2, y + 2, dataMatrix);
}

// Add all codewords (data and error correction) to barcode
void addCodeWords(unsigned char dataMatrix[], unsigned char dataCodeWords[])
{
  if (dataCodeWords[0] & 0x80) setDataMatrixBit(21, 5, dataMatrix);
  if (dataCodeWords[0] & 0x40) setDataMatrixBit(22, 5, dataMatrix);
  if (dataCodeWords[0] & 0x20) setDataMatrixBit(21, 6, dataMatrix);
  if (dataCodeWords[0] & 0x10) setDataMatrixBit(22, 6, dataMatrix);
  if (dataCodeWords[0] & 0x08) setDataMatrixBit(1, 4, dataMatrix);
  if (dataCodeWords[0] & 0x04) setDataMatrixBit(21, 7, dataMatrix);
  if (dataCodeWords[0] & 0x02) setDataMatrixBit(22, 7, dataMatrix);
  if (dataCodeWords[0] & 0x01) setDataMatrixBit(1, 5, dataMatrix);

  addStandardCWBlock(1, 1, dataCodeWords[1], dataMatrix);

  if (dataCodeWords[2] & 0x80) setDataMatrixBit(5, 21, dataMatrix);
  if (dataCodeWords[2] & 0x40) setDataMatrixBit(6, 21, dataMatrix);
  if (dataCodeWords[2] & 0x20) setDataMatrixBit(5, 22, dataMatrix);
  if (dataCodeWords[2] & 0x10) setDataMatrixBit(6, 22, dataMatrix);
  if (dataCodeWords[2] & 0x08) setDataMatrixBit(7, 22, dataMatrix);
  if (dataCodeWords[2] & 0x04) setDataMatrixBit(3, 1, dataMatrix);
  if (dataCodeWords[2] & 0x02) setDataMatrixBit(4, 1, dataMatrix);
  if (dataCodeWords[2] & 0x01) setDataMatrixBit(5, 1, dataMatrix);

  if (dataCodeWords[3] & 0x80) setDataMatrixBit(8, 22, dataMatrix);
  if (dataCodeWords[3] & 0x40) setDataMatrixBit(9, 22, dataMatrix);
  if (dataCodeWords[3] & 0x20) setDataMatrixBit(6, 1, dataMatrix);
  if (dataCodeWords[3] & 0x10) setDataMatrixBit(7, 1, dataMatrix);
  if (dataCodeWords[3] & 0x08) setDataMatrixBit(8, 1, dataMatrix);
  if (dataCodeWords[3] & 0x04) setDataMatrixBit(6, 2, dataMatrix);
  if (dataCodeWords[3] & 0x02) setDataMatrixBit(7, 2, dataMatrix);
  if (dataCodeWords[3] & 0x01) setDataMatrixBit(8, 2, dataMatrix);

  addStandardCWBlock(4, 2, dataCodeWords[4], dataMatrix);
  addStandardCWBlock(2, 4, dataCodeWords[5], dataMatrix);

  if (dataCodeWords[6] & 0x80) setDataMatrixBit(22, 8, dataMatrix);
  if (dataCodeWords[6] & 0x40) setDataMatrixBit(1, 6, dataMatrix);
  if (dataCodeWords[6] & 0x20) setDataMatrixBit(22, 9, dataMatrix);
  if (dataCodeWords[6] & 0x10) setDataMatrixBit(1, 7, dataMatrix);
  if (dataCodeWords[6] & 0x08) setDataMatrixBit(2, 7, dataMatrix);
  if (dataCodeWords[6] & 0x04) setDataMatrixBit(22, 10, dataMatrix);
  if (dataCodeWords[6] & 0x02) setDataMatrixBit(1, 8, dataMatrix);
  if (dataCodeWords[6] & 0x01) setDataMatrixBit(2, 8, dataMatrix);

  if (dataCodeWords[7] & 0x80) setDataMatrixBit(21, 13, dataMatrix);
  if (dataCodeWords[7] & 0x40) setDataMatrixBit(22, 13, dataMatrix);
  if (dataCodeWords[7] & 0x20) setDataMatrixBit(21, 14, dataMatrix);
  if (dataCodeWords[7] & 0x10) setDataMatrixBit(22, 14, dataMatrix);
  if (dataCodeWords[7] & 0x08) setDataMatrixBit(1, 12, dataMatrix);
  if (dataCodeWords[7] & 0x04) setDataMatrixBit(21, 15, dataMatrix);
  if (dataCodeWords[7] & 0x02) setDataMatrixBit(22, 15, dataMatrix);
  if (dataCodeWords[7] & 0x01) setDataMatrixBit(1, 13, dataMatrix);

  addStandardCWBlock(1, 9, dataCodeWords[8], dataMatrix);
  addStandardCWBlock(3, 7, dataCodeWords[9], dataMatrix);
  addStandardCWBlock(5, 5, dataCodeWords[10], dataMatrix);
  addStandardCWBlock(7, 3, dataCodeWords[11], dataMatrix);
  addStandardCWBlock(9, 1, dataCodeWords[12], dataMatrix);

  if (dataCodeWords[13] & 0x80) setDataMatrixBit(13, 21, dataMatrix);
  if (dataCodeWords[13] & 0x40) setDataMatrixBit(14, 21, dataMatrix);
  if (dataCodeWords[13] & 0x20) setDataMatrixBit(13, 22, dataMatrix);
  if (dataCodeWords[13] & 0x10) setDataMatrixBit(14, 22, dataMatrix);
  if (dataCodeWords[13] & 0x08) setDataMatrixBit(15, 22, dataMatrix);
  if (dataCodeWords[13] & 0x04) setDataMatrixBit(11, 1, dataMatrix);
  if (dataCodeWords[13] & 0x02) setDataMatrixBit(12, 1, dataMatrix);
  if (dataCodeWords[13] & 0x01) setDataMatrixBit(13, 1, dataMatrix);

  if (dataCodeWords[14] & 0x80) setDataMatrixBit(16, 22, dataMatrix);
  if (dataCodeWords[14] & 0x40) setDataMatrixBit(17, 22, dataMatrix);
  if (dataCodeWords[14] & 0x20) setDataMatrixBit(14, 1, dataMatrix);
  if (dataCodeWords[14] & 0x10) setDataMatrixBit(15, 1, dataMatrix);
  if (dataCodeWords[14] & 0x08) setDataMatrixBit(16, 1, dataMatrix);
  if (dataCodeWords[14] & 0x04) setDataMatrixBit(14, 2, dataMatrix);
  if (dataCodeWords[14] & 0x02) setDataMatrixBit(15, 2, dataMatrix);
  if (dataCodeWords[14] & 0x01) setDataMatrixBit(16, 2, dataMatrix);

  addStandardCWBlock(12, 2, dataCodeWords[15], dataMatrix);
  addStandardCWBlock(10, 4, dataCodeWords[16], dataMatrix);
  addStandardCWBlock(8, 6, dataCodeWords[17], dataMatrix);
  addStandardCWBlock(6, 8, dataCodeWords[18], dataMatrix);
  addStandardCWBlock(4, 10, dataCodeWords[19], dataMatrix);
  addStandardCWBlock(2, 12, dataCodeWords[20], dataMatrix);

  if (dataCodeWords[21] & 0x80) setDataMatrixBit(22, 16, dataMatrix);
  if (dataCodeWords[21] & 0x40) setDataMatrixBit(1, 14, dataMatrix);
  if (dataCodeWords[21] & 0x20) setDataMatrixBit(22, 17, dataMatrix);
  if (dataCodeWords[21] & 0x10) setDataMatrixBit(1, 15, dataMatrix);
  if (dataCodeWords[21] & 0x08) setDataMatrixBit(2, 15, dataMatrix);
  if (dataCodeWords[21] & 0x04) setDataMatrixBit(22, 18, dataMatrix);
  if (dataCodeWords[21] & 0x02) setDataMatrixBit(1, 16, dataMatrix);
  if (dataCodeWords[21] & 0x01) setDataMatrixBit(2, 16, dataMatrix);

  if (dataCodeWords[22] & 0x80) setDataMatrixBit(1, 20, dataMatrix);
  if (dataCodeWords[22] & 0x40) setDataMatrixBit(1, 21, dataMatrix);
  if (dataCodeWords[22] & 0x20) setDataMatrixBit(1, 22, dataMatrix);
  if (dataCodeWords[22] & 0x10) setDataMatrixBit(19, 1, dataMatrix);
  if (dataCodeWords[22] & 0x08) setDataMatrixBit(20, 1, dataMatrix);
  if (dataCodeWords[22] & 0x04) setDataMatrixBit(21, 1, dataMatrix);
  if (dataCodeWords[22] & 0x02) setDataMatrixBit(22, 1, dataMatrix);
  if (dataCodeWords[22] & 0x01) setDataMatrixBit(22, 2, dataMatrix);

  addStandardCWBlock(1, 17, dataCodeWords[23], dataMatrix);
  addStandardCWBlock(3, 15, dataCodeWords[24], dataMatrix);
  addStandardCWBlock(5, 13, dataCodeWords[25], dataMatrix);
  addStandardCWBlock(7, 11, dataCodeWords[26], dataMatrix);
  addStandardCWBlock(9, 9, dataCodeWords[27], dataMatrix);
  addStandardCWBlock(11, 7, dataCodeWords[28], dataMatrix);
  addStandardCWBlock(13, 5, dataCodeWords[29], dataMatrix);
  addStandardCWBlock(15, 3, dataCodeWords[30], dataMatrix);
  addStandardCWBlock(17, 1, dataCodeWords[31], dataMatrix);
  addStandardCWBlock(20, 2, dataCodeWords[32], dataMatrix);
  addStandardCWBlock(18, 4, dataCodeWords[33], dataMatrix);
  addStandardCWBlock(16, 6, dataCodeWords[34], dataMatrix);
  addStandardCWBlock(14, 8, dataCodeWords[35], dataMatrix);
  addStandardCWBlock(12, 10, dataCodeWords[36], dataMatrix);
  addStandardCWBlock(10, 12, dataCodeWords[37], dataMatrix);
  addStandardCWBlock(8, 14, dataCodeWords[38], dataMatrix);
  addStandardCWBlock(6, 16, dataCodeWords[39], dataMatrix);
  addStandardCWBlock(4, 18, dataCodeWords[40], dataMatrix);
  addStandardCWBlock(2, 20, dataCodeWords[41], dataMatrix);
  addStandardCWBlock(7, 19, dataCodeWords[42], dataMatrix);
  addStandardCWBlock(9, 17, dataCodeWords[43], dataMatrix);
  addStandardCWBlock(11, 15, dataCodeWords[44], dataMatrix);
  addStandardCWBlock(13, 13, dataCodeWords[45], dataMatrix);
  addStandardCWBlock(15, 11, dataCodeWords[46], dataMatrix);
  addStandardCWBlock(17, 9, dataCodeWords[47], dataMatrix);
  addStandardCWBlock(19, 7, dataCodeWords[48], dataMatrix);
  addStandardCWBlock(20, 10, dataCodeWords[49], dataMatrix);
  addStandardCWBlock(18, 12, dataCodeWords[50], dataMatrix);
  addStandardCWBlock(16, 14, dataCodeWords[51], dataMatrix);
  addStandardCWBlock(14, 16, dataCodeWords[52], dataMatrix);
  addStandardCWBlock(12, 18, dataCodeWords[53], dataMatrix);
  addStandardCWBlock(10, 20, dataCodeWords[54], dataMatrix);
  addStandardCWBlock(15, 19, dataCodeWords[55], dataMatrix);
  addStandardCWBlock(17, 17, dataCodeWords[56], dataMatrix);
  addStandardCWBlock(19, 15, dataCodeWords[57], dataMatrix);
  addStandardCWBlock(20, 18, dataCodeWords[58], dataMatrix);
  addStandardCWBlock(18, 20, dataCodeWords[59], dataMatrix);
}

// Set bit in 1D array index from 2D coordinates
void setDataMatrixBit(uint8_t x, uint8_t y, unsigned char dataMatrix[])
{
  uint8_t arrayOffset = ((x / 8) * MATRIXCOLS) + y;
  uint8_t bitOffset = x % 8;

  dataMatrix[arrayOffset] |= (1 << bitOffset);
}

void displayDataMatrix(String text)
{
  uint8_t dataCodeWords[TOTALCWCOUNT] = { 0 };
  uint8_t dataMatrix[(MATRIXROWS * MATRIXCOLS) / 8] = { 0 };

  // LED matrices are not laid out in sequential order on PCB, therefore we need a scramble
  const uint8_t tileScramble[9] = { 0, 1, 2, 5, 3, 4, 6, 7, 8 };

  int numDataCW = encodeDataCodeWords(text, dataCodeWords);
  padDataCodeWords(numDataCW, dataCodeWords);
  addErrorCorrectCW(dataCodeWords);
  addFinderPattern(dataMatrix);
  addCodeWords(dataMatrix, dataCodeWords);

  // Driver is 16x8, so we need to output a row from two 8x8 tiles each time
  int driver = 0;
  for (int offset = 0; offset < 10; offset += 2)
  {
    Wire.beginTransmission(HT16K33_START_ADDR + driver);
    Wire.write(0x00);

    for (int b = 0; b < 8; b++)
    {
      Wire.write(dataMatrix[(tileScramble[offset] * 8) + b]);

      if (offset < 8)
        Wire.write(dataMatrix[(tileScramble[offset + 1] * 8) + b]);
      else
        Wire.write(0x00);
    }
    Wire.endTransmission();
    driver++;
  }

  if (autoBrightness) sampleLight();
  if (updateBrightnessFlag)
  {
    setDisplayBrightness(brightness);
    updateBrightnessFlag = false;
  }
}

bool syncTime()
{
  if (WiFi.status() != WL_CONNECTED) return false;

  const uint8_t NTP_PACKET_SIZE = 48;     // NTP time stamp is in the first 48 bytes of the message
  uint8_t packetBuffer[NTP_PACKET_SIZE];  // Buffer to hold incoming and outgoing packets
  IPAddress serverIP;

  // Get a random server from the NTP server pool
  WiFi.hostByName(NTP_POOL, serverIP);

  // Set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);

  // Initialize values needed to form NTP request
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;            // Stratum, or type of clock
  packetBuffer[2] = 6;            // Polling Interval
  packetBuffer[3] = 0xEC;         // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;

  // Send packet requesting a timestamp
  WiFiUDP udp;
  udp.begin(8888);
  while (udp.parsePacket() > 0) yield();
  udp.beginPacket(serverIP, 123);
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();

  // Wait for a response
  int timeout = 0;
  while (!udp.parsePacket() && (timeout < 100))
  {
    delay(10);
    timeout++;
  }

  // No response
  if (timeout >= 100) return false;

  // Read the packet into the buffer
  udp.read(packetBuffer, NTP_PACKET_SIZE);
  uint32_t highWord = word(packetBuffer[40], packetBuffer[41]);
  uint32_t lowWord = word(packetBuffer[42], packetBuffer[43]);
  uint32_t secsSince1900 = highWord << 16 | lowWord;
  uint32_t epoch = secsSince1900 - 2208988800UL;
  udp.stop();

  // Write time to RTC
  RTC.set((time_t)epoch, RTC_CLOCK);
  setTime((time_t)epoch);

  // Schedule next NTP update (24 hours)
  ntpUpdate_tckr.once(86400, setUpdateTimeFlag);

  Serial.print("Got NTP time: ");
  Serial.print(hour((time_t)epoch)); Serial.print(":");
  Serial.print(minute((time_t)epoch)); Serial.print(":");
  Serial.print(second((time_t)epoch)); Serial.print(" ");
  Serial.print(month((time_t)epoch)); Serial.print("/");
  Serial.print(day((time_t)epoch)); Serial.print("/");
  Serial.println(year((time_t)epoch));

  // We got the time!
  return true;
}

void displayTime()
{
  if (sleepMode) return;

  char timestr[17];
  time_t local = myTZ.toLocal(now(), &tcr);

  sprintf(timestr, "%02d:%02d:%02d %02d/%02d/%04d", (twentyFourHourTime ? hour(local) : hourFormat12(local)), minute(local),
                                                    second(local), month(local), day(local), year(local));
  displayDataMatrix(String(timestr));
}

void sendToLEDDriver(uint8_t addr, uint8_t data)
{
  Wire.beginTransmission(addr);
  Wire.write(data);
  Wire.endTransmission();
}

void sendToAllLEDDrivers(uint8_t data)
{
  for (int driver = 0; driver < 5; driver++) sendToLEDDriver(HT16K33_START_ADDR + driver, data);
}

void initDisplay() { sendToAllLEDDrivers(HT16K33_OSC_ON); sendToAllLEDDrivers(HT16K33_DISPLAY_ON); }
void setDisplayBrightness(uint8_t brightness) { sendToAllLEDDrivers(HT16K33_BRIGHTNESS | constrain(brightness, 0, 15)); }
void displayOff() { sendToAllLEDDrivers(HT16K33_DISPLAY_OFF); }
void displayOn() { sendToAllLEDDrivers(HT16K33_DISPLAY_ON); }
void sleepDisplay() { sendToAllLEDDrivers(HT16K33_OSC_OFF); }
void wakeupDisplay() { sendToAllLEDDrivers(HT16K33_OSC_ON); }

// HT16K33's have a built-in blink function, but their internal oscillators vary
// enough to where each matrix gets out of sync, so we'll do our own blinking.
void blinkDisplay(bool enable)
{
  if (enable)
    displayBlink_tckr.attach_ms(BLINK_INTERVAL, doDisplayBlink);
  else
  {
    displayBlink_tckr.detach();
    displayOn();
  }
}

void doDisplayBlink()
{
  blinkState = !blinkState;
  if (blinkState)
    displayOff();
  else
    displayOn();
}
/*
void sampleLight()
{
  static uint8_t index = 0;
  uint8_t newBrightness;

  lightTotal -= lightSamples[index];
  lightSamples[index] = analogRead(A0);
  lightTotal += lightSamples[index];
  uint16_t lightAvg = roundBy5(lightTotal / LDR_SAMPLES);

  newBrightness = map(lightAvg, LDR_CONSTRAIN, 1024, 15, 0);
  newBrightness = constrain(newBrightness, 0, 15);

  Serial.print("Light sample: ");
  Serial.print(lightSamples[index]);
  Serial.print(", Avg: ");
  Serial.print(lightAvg);
  Serial.print(", Brightness (constrained): ");
  Serial.println(newBrightness);

  if (newBrightness != brightness)
  {
    brightness = newBrightness;
    updateBrightnessFlag = true;
  }

  if (++index >= LDR_SAMPLES) index = 0;
}
*/

void sampleLight()
{
  uint16_t lightSample = analogRead(A0);
  lightValue = (lastLightValue * (LIGHT_FILTER_PERCENT - 1)) +
               (lightSample * LIGHT_FILTER_PERCENT);
  lastLightValue = lightValue;

  uint8_t newBrightness = map(lightValue, LDR_CONSTRAIN, 1024, 15, 0);
  newBrightness = constrain(newBrightness, 0, 15);

  Serial.print("Light sample: ");
  Serial.print(lightSample);
  Serial.print(", Avg: ");
  Serial.print(lightValue);
  Serial.print(", Brightness (constrained): ");
  Serial.println(newBrightness);

  if (newBrightness != brightness)
  {
    brightness = newBrightness;
    updateBrightnessFlag = true;
  }
}

void sleepCheck()
{
  uint8_t sr = RTC.readStatusReg();

  if (sr & 0x1)  // ALARM1 Flag (A1F)
  {
    sleepMode = true;
    sleepDisplay();
    RTC.clearAlarmFlag(RTC_ALARM1);
  }
  if (sr & 0x2)  // ALARM2 Flag (A2F)
  {
    sleepMode = false;
    wakeupDisplay();
    RTC.clearAlarmFlag(RTC_ALARM2);
  }
}

void initSettings()
{
  EEPROM.write(SETTING_INITIALIZED,   0x55);  // EEPROM initialized
  EEPROM.write(SETTING_AUTOBRIGHT,    1);     // auto brightness
  EEPROM.write(SETTING_BRIGHTNESS,    7);     // brightness
  EEPROM.write(SETTING_SLEEPENABLE,   0);     // sleep enable
  EEPROM.write(SETTING_SLEEPSTARTHR,  23);    // sleep start hour
  EEPROM.write(SETTING_SLEEPSTARTMIN, 0);     // sleep start minute
  EEPROM.write(SETTING_SLEEPENDHR,    7);     // sleep end hour
  EEPROM.write(SETTING_SLEEPENDMIN,   0);     // sleep end minute
  EEPROM.write(SETTING_24HOUR,        0);     // 24 hour time
  EEPROM.write(SETTING_TIMEZONESTD,   5);    // timezone STD (in hours)
  EEPROM.write(SETTING_TIMEZONEDST,   4);    // timezone DST (in hours)
  EEPROM.commit();
}

void readSettings()
{
  if (EEPROM.read(SETTING_INITIALIZED) != 0x55) initSettings();

  autoBrightness = EEPROM.read(SETTING_AUTOBRIGHT);
  brightness = EEPROM.read(SETTING_BRIGHTNESS);
  updateBrightnessFlag = true;
  sleepEnable = EEPROM.read(SETTING_SLEEPENABLE);
  twentyFourHourTime = EEPROM.read(SETTING_24HOUR);
  timeSTD = (TimeChangeRule){"STD", First, Sun, Nov, 2, EEPROM.read(SETTING_TIMEZONESTD) * -60};
  timeDST = (TimeChangeRule){"DST", Second, Sun, Mar, 2, EEPROM.read(SETTING_TIMEZONEDST) * -60};
  myTZ = Timezone(timeDST, timeSTD);

  if (sleepEnable)
  {
    tmElements_t tmStart, tmEnd;
    time_t utc = now();
    time_t alarm;
    breakTime(utc, tmStart);
    breakTime(utc, tmEnd);
    tmStart.Second = 0;
    tmEnd.Second = 0;

    tmStart.Hour = EEPROM.read(SETTING_SLEEPSTARTHR);
    tmStart.Minute = EEPROM.read(SETTING_SLEEPSTARTMIN);
    tmEnd.Hour = EEPROM.read(SETTING_SLEEPENDHR);
    tmEnd.Minute = EEPROM.read(SETTING_SLEEPENDMIN);

    // Write and enable Alarm1 on RTC for sleep start
    alarm = makeTime(tmStart);
    alarm = myTZ.toUTC(alarm);
    breakTime(alarm, tmStart);
    RTC.write(tmStart, RTC_ALARM1);
    RTC.clearAlarmFlag(RTC_ALARM1);
    RTC.enableAlarm(RTC_ALARM1);

    // Write and enable Alarm2 on RTC for sleep end
    alarm = makeTime(tmEnd);
    alarm = myTZ.toUTC(alarm);
    breakTime(alarm, tmEnd);
    RTC.write(tmEnd, RTC_ALARM2);
    RTC.clearAlarmFlag(RTC_ALARM2);
    RTC.enableAlarm(RTC_ALARM2);
  }

  else
  {
    RTC.disableAlarm(RTC_ALARM1);
    RTC.disableAlarm(RTC_ALARM2);
    RTC.clearAlarmFlag(RTC_ALARM1);
    RTC.clearAlarmFlag(RTC_ALARM2);
  }
}

void getSettings()
{
  String response;
  IPAddress ip = WiFi.localIP();

  response = String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]) + ",";

  for (uint8_t i = 1; i < NUM_OF_SETTINGS; i++)
  {
    response += EEPROM.read(i);
    response += ",";
  }

  server.send(200, "text/html", response);
}

void saveSettings()
{
  bool _autoBrightness = (server.arg("autoBrightness").toInt() ? true : false);
  uint8_t _brightness = constrain(server.arg("brightness").toInt(), 0, 15);
  bool _sleepEnable = (server.arg("sleepEnable").toInt() ? true : false);
  uint8_t _sleepStartHr = constrain(server.arg("sleepStartHr").toInt(), 0, 23);
  uint8_t _sleepStartMin = constrain(server.arg("sleepStartMin").toInt(), 0, 59);
  uint8_t _sleepEndHr = constrain(server.arg("sleepEndHr").toInt(), 0, 23);
  uint8_t _sleepEndMin = constrain(server.arg("sleepEndMin").toInt(), 0, 59);
  bool _twentyFourHourTime = (server.arg("twentyFourHourTime").toInt() ? true : false);
  int8_t _timezoneSTD = server.arg("timezoneSTD").toInt();
  int8_t _timezoneDST = server.arg("timezoneDST").toInt();

  EEPROM.write(SETTING_AUTOBRIGHT, _autoBrightness);    // auto brightness
  EEPROM.write(SETTING_BRIGHTNESS, _brightness);        // brightness
  EEPROM.write(SETTING_SLEEPENABLE, _sleepEnable);      // sleep enable
  EEPROM.write(SETTING_SLEEPSTARTHR, _sleepStartHr);    // sleep start hour
  EEPROM.write(SETTING_SLEEPSTARTMIN, _sleepStartMin);  // sleep start minute
  EEPROM.write(SETTING_SLEEPENDHR, _sleepEndHr);        // sleep end hour
  EEPROM.write(SETTING_SLEEPENDMIN, _sleepEndMin);      // sleep end minute
  EEPROM.write(SETTING_24HOUR, _twentyFourHourTime);    // 24 hour time
  EEPROM.write(SETTING_TIMEZONESTD, _timezoneSTD);      // timezone STD
  EEPROM.write(SETTING_TIMEZONEDST, _timezoneDST);      // timezone DST
  EEPROM.commit();

  readSettings();

  server.send(200, "text/html");
}

void wifiConfigModeCallback(WiFiManager *myWiFiManager)
{
  displayDataMatrix(F("Config Wifi, Connect to BarcodeClock"));
  blinkDisplay(true);
}

void httpUpdateStart() { displayDataMatrix(F("Firmware update in progress")); }
void httpUpdateFail() { displayDataMatrix(F("Firmware update failed!")); blinkDisplay(true); }

bool handleHTTPFileRead(String path)
{
  if (path.endsWith("/")) path += "index.htm";

  String dataType = "text/plain";
  if (path.endsWith(".htm")) dataType = "text/html";
  else if (path.endsWith(".css")) dataType = "text/css";

  if (SPIFFS.exists(path))
  {
    File file = SPIFFS.open(path, "r");
    server.sendHeader("Connection", "close");
    server.streamFile(file, dataType);
    file.close();
    return true;
  }
  return false;
}
/*
int roundBy5(int num)
{
  const int8_t round5delta[5] = {0, -1, -2, 2, 1};
  return num + round5delta[num % 5];
}
*/
