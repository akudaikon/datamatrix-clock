var autoBrightness = document.getElementById('autoBrightness'),
    brightnessDiv = document.getElementById('brightnessDiv'),
    brightnessRange = document.getElementById('brightnessRange'),
    autoSleep = document.getElementById('autoSleep'),
    sleepDiv = document.getElementById('sleepDiv'),
    sleepStartHour = document.getElementById('sleepStartHour'),
    sleepStartMinute = document.getElementById('sleepStartMinute'),
    sleepStartAMPM = document.getElementById('sleepStartAMPM'),
    sleepEndHour = document.getElementById('sleepEndHour'),
    sleepEndMinute = document.getElementById('sleepEndMinute'),
    sleepEndAMPM = document.getElementById('sleepEndAMPM'),
    twentyFourHour = document.getElementById('24HourTime'),
    timezone = document.getElementById('timezone'),
    ip = document.getElementById('ip'),
    saveSettingsBtn = document.getElementById('saveSettingsBtn'),
    restartBtn = document.getElementById('restartBtn'),
    messageModalIcon = document.getElementById('messageModalIcon'),
    messageModalText = document.getElementById('messageModalText'),
    messageModalProgress = document.getElementById('messageModalProgress');

checkMax = function() {
  if (this.value.length > 2) this.value = this.value.slice(0, 2);
};

checkNum = function() {
  if (parseInt(this.value) > parseInt(this.max)) this.value = this.max;
  this.value = pad(this.value, 2);
};

pad = function(str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

sleepStartHour.addEventListener("input", checkMax);
sleepStartHour.addEventListener("change", checkNum);
sleepStartMinute.addEventListener("input", checkMax);
sleepStartMinute.addEventListener("change", checkNum);
sleepEndHour.addEventListener("input", checkMax);
sleepEndHour.addEventListener("change", checkNum);
sleepEndMinute.addEventListener("input", checkMax);
sleepEndMinute.addEventListener("change", checkNum);

autoBrightness.onchange = function() {
  brightnessDiv.style.display = (!this.checked ? 'block' : 'none');
};

autoSleep.onchange = function() {
  sleepDiv.style.display = (this.checked ? 'block' : 'none');
};

$(function(){
  $('.spinner .btn:first-of-type').on('click', function() {
    var btn = $(this);
    var input = btn.closest('.spinner').find('input');
    if (input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
      input.val(pad(parseInt(input.val(), 10) + 1, 2));
    } else {
      btn.next("disabled", true);
    }
  });
  $('.spinner .btn:last-of-type').on('click', function() {
    var btn = $(this);
    var input = btn.closest('.spinner').find('input');
    if (input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
      input.val(pad(parseInt(input.val(), 10) - 1, 2));
    } else {
      btn.prev("disabled", true);
    }
  });
})

getSettings = function() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        console.log(">" + xhttp.responseText);
        var settings = xhttp.responseText.split(",");

        autoBrightness.checked = (settings[1] == "1" ? true : false);
        brightnessRange.value = settings[2];
        autoSleep.checked = (settings[3] == "1" ? true : false);

        if (settings[4] == 0) {
          sleepStartHour.value = 12;
          $('#sleepStartAMPM').val(0);
        }
        else if (settings[4] > 12) {
          sleepStartHour.value = pad(settings[4] - 12, 2);
          $('#sleepStartAMPM').val(1);
        } else {
          sleepStartHour.value = pad(settings[4], 2);
          $('#sleepStartAMPM').val(0);
        }
        sleepStartMinute.value = pad(settings[5], 2);

        if (settings[6] == 0) {
          sleepEndHour.value = 12;
          $('#sleepEndAMPM').val(0);
        }
        else if (settings[6] > 12) {
          sleepEndHour.value = pad(settings[6] - 12, 2);
          $('#sleepEndAMPM').val(1);
        } else {
          sleepEndHour.value = pad(settings[6], 2);
          $('#sleepEndAMPM').val(0);
        }
        sleepEndMinute.value = pad(settings[7], 2);
        $('#sleepStartAMPM').selectpicker('refresh');
        $('#sleepEndAMPM').selectpicker('refresh');

        twentyFourHour.checked = (settings[8] == "1" ? true : false);
        
        $('#timezone').val(settings[9]);
        $('#timezone').selectpicker('refresh');

        ip.innerHTML = "<small>IP Address: " + settings[0] + "</small>";

        sleepDiv.style.display = (autoSleep.checked ? 'block' : 'none');
        brightnessDiv.style.display = (!autoBrightness.checked ? 'block' : 'none');

        $('#messageModal').modal('hide');
      } else {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
        messageModalText.innerHTML = "<h4>Error getting settings!</h4>Please refresh and try again<br>(Error " + xhttp.status + ")";
        messageModalProgress.style.display = "none";
      }
    }
  };

  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-time'></span></h1>";
  messageModalText.innerHTML = "<h4>Getting Settings...</h4>";
  messageModalProgress.style.display = "block";
  $('#messageModal').modal('show');

  xhttp.open("GET", "getsettings", true);
  xhttp.send();
};

saveSettingsBtn.onclick = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-ok' style='color: green'></span></h1>";
        messageModalText.innerHTML = "<h4>Settings saved successfully!</h4>";
        messageModalProgress.style.display = "none";
      } else {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
        messageModalText.innerHTML = "<h4>Error saving settings!</h4>Please try again<br>(Error " + xhttp.status + ")";
        messageModalProgress.style.display = "none";
      }
      setTimeout(function() { $('#messageModal').modal('hide'); }, 2000);
    }
  };

  var startHour = parseInt(sleepStartHour.value);
  var endHour = parseInt(sleepEndHour.value);

  if (sleepStartAMPM.value == 1 && startHour < 12) startHour += 12;
  if (sleepStartAMPM.value == 0 && startHour == 12) startHour -= 12;
  if (sleepEndAMPM.value == 1 && endHour < 12) endHour += 12;
  if (sleepEndAMPM.value == 0 && endHour == 12) endHour -= 12;

  var settings = "autoBrightness=" + (autoBrightness.checked ? 1 : 0) + "&"
               + "brightness=" + brightnessRange.value + "&"
               + "sleepEnable=" + (autoSleep.checked ? 1 : 0) + "&"
               + "sleepStartHr=" + startHour + "&"
               + "sleepStartMin=" + sleepStartMinute.value + "&"
               + "sleepEndHr=" + endHour + "&"
               + "sleepEndMin=" + sleepEndMinute.value + "&"
               + "twentyFourHourTime=" + (twentyFourHour.checked ? 1 : 0) + "&"
               + "timezoneSTD=" + $('#timezone option:selected').val() + "&"
               + "timezoneDST=" +  (parseInt($('#timezone option:selected').val()) - 1);

  console.log("<" + settings);

  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-time'></span></h1>";
  messageModalText.innerHTML = "<h4>Saving settings...</h4>";
  messageModalProgress.style.display = "block";
  $('#messageModal').modal('show');

  xhttp.open("POST", "savesettings", true);
  xhttp.send(settings);
}

restartBtn.onclick = function() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status != 200) {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
        messageModalText.innerHTML = "<h4>Error restarting clock!</h4>Please try again<br>(Error " + xhttp.status + ")";
        messageModalProgress.style.display = "none";
        setTimeout(function() { $('#messageModal').modal('hide'); }, 2000);
      }
    }
  };

  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-repeat' style='color: black'></span></h1>";
  messageModalText.innerHTML = "<h4>Restarting Barcode Clock...</h4>Please refresh after clock has restarted";
  messageModalProgress.style.display = "block";
  $('#messageModal').modal('show');

  xhttp.open("GET", "/restart", true);
  xhttp.send();
}

getSettings();