/*
 * DS1337RTC.h - library for DS1337 RTC

  By Eric Trombly

  modified from DS1307RTC.h (c) Michael Margolis 2009
  This library is intended to be uses with Arduino Time.h library functions

  The library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  7 Jul 2011 - Initial release
 */

#include <Wire.h>
#include "DS1337RTC.h"

uint8_t controlRegister = 0x18; // default settings on start

DS1337RTC::DS1337RTC()
{
  // Only initialize Wire if not already initialized
  //Wire.begin();
}

// PUBLIC FUNCTIONS
time_t DS1337RTC::get(int address)   // Aquire data from buffer and convert to time_t
{
  tmElements_t tm;
  read(tm, address);
  return(makeTime(tm));
}

void  DS1337RTC::set(time_t t, int address) // Convert from time_t and save to rtc
{
  tmElements_t tm;
  breakTime(t, tm);
  stopClock();
  write(tm, address);
  startClock();
}

time_t DS1337RTC::sync()  // Function to use for Time.h setSyncProvider
{
  return(get(RTC_CLOCK));
}

void DS1337RTC::read(tmElements_t &tm, int address) // Aquire data from the RTC chip in BCD format
{
  int numberBytes;

  switch (address)
  {
    case RTC_CLOCK: numberBytes = tmNbrFields; break;
    case RTC_ALARM1: numberBytes = 4; break;
    case RTC_ALARM2: numberBytes = 3; break;
  }

  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(address);
  Wire.endTransmission();

  Wire.requestFrom(DS1337_CTRL_ID, numberBytes);

  if (address != RTC_ALARM2)   // alarm 2 doesn't have a seconds field
    tm.Second = bcd2dec(Wire.read());

  tm.Minute = bcd2dec(Wire.read());
  tm.Hour =   bcd2dec(Wire.read());

  if (address == RTC_ALARM1 || address == RTC_ALARM2)  // the alarms don't have a wday, day, month, or year field
  {
    tm.Day = bcd2dec(Wire.read());
    tm.Month = 0;
    tm.Year = 0;
  }
  else
  {
    tm.Wday = bcd2dec(Wire.read());
    tm.Day = bcd2dec(Wire.read());
    tm.Month = bcd2dec(Wire.read());
    tm.Year = y2kYearToTm((bcd2dec(Wire.read())));
  }
}

void DS1337RTC::write(tmElements_t &tm, int address)
{
  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(address);

  if (address != RTC_ALARM2)  // alarm 2 doesn't have a seconds field
    Wire.write(dec2bcd(tm.Second));

  Wire.write(dec2bcd(tm.Minute));
  Wire.write(dec2bcd(tm.Hour));

  if (address == RTC_ALARM1 || address == RTC_ALARM2)  // the alarms don't have a wday, day, month, or year field
    Wire.write(0x80);   // mask day for alarm (bit 7)
  else
  {
    Wire.write(dec2bcd(tm.Wday));
    Wire.write(dec2bcd(tm.Day));
    Wire.write(dec2bcd(tm.Month));
    Wire.write(dec2bcd(tmYearToY2k(tm.Year)));
  }
  Wire.endTransmission();
}

uint8_t DS1337RTC::readControlReg()
{
  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(CONTROL_ADDRESS);
  Wire.endTransmission();

  Wire.requestFrom(DS1337_CTRL_ID, 1);
  uint8_t cr = Wire.read();

  return cr;
}

void DS1337RTC::enableAlarm(int address)  //turn on the A1IE or A2IE bit
{
  uint8_t cr = readControlReg();

  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(CONTROL_ADDRESS);

  if (address == RTC_ALARM1)
    cr |= 0x01;
  else
    cr |= 0x02;

  Wire.write(cr);
  Wire.endTransmission();
}

void DS1337RTC::disableAlarm(int address)  // turn off the A1IE or A2IE bit
{
  uint8_t cr = readControlReg();

  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(CONTROL_ADDRESS);

  if (address == RTC_ALARM1)
    cr &= ~0x01;
  else
    cr &= ~0x02;

  Wire.write(cr);
  Wire.endTransmission();
}

void DS1337RTC::setSquareWaveRate(int rate)  // set RS bits
{
  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(CONTROL_ADDRESS);
  Wire.endTransmission();

  Wire.requestFrom(DS1337_CTRL_ID, 1);
  uint8_t cr = Wire.read();

  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(CONTROL_ADDRESS);

  cr &= ~(0x03 << 3);
  cr |= (rate & 0x03) << 3;

  Wire.write(cr);
  Wire.endTransmission();
}

uint8_t DS1337RTC::readStatusReg()
{
  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(STATUS_ADDRESS);
  Wire.endTransmission();

  Wire.requestFrom(DS1337_CTRL_ID, 1);
  uint8_t sr = Wire.read();

  return sr;
}

void DS1337RTC::clearAlarmFlag(int address)  // reset the A1F and A2F bits
{
  uint8_t sr = readStatusReg();

  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(STATUS_ADDRESS);

  if (address == RTC_ALARM1)
    sr &= ~0x01;
  else
    sr &= ~0x02;

  Wire.write(sr);
  Wire.endTransmission();
}

// PRIVATE FUNCTIONS

// Convert Decimal to Binary Coded Decimal (BCD)
uint8_t DS1337RTC::dec2bcd(uint8_t num)
{
  return ((num/10 * 16) + (num % 10));
}

// Convert Binary Coded Decimal (BCD) to Decimal
uint8_t DS1337RTC::bcd2dec(uint8_t num)
{
  return ((num/16 * 10) + (num % 16));
}

void DS1337RTC::stopClock()
{
  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(STATUS_ADDRESS);
  Wire.write(0x80);
  Wire.endTransmission();
}

void DS1337RTC::startClock()
{
  Wire.beginTransmission(DS1337_CTRL_ID);
  Wire.write(STATUS_ADDRESS);
  Wire.write(0x00);
  Wire.endTransmission();
}

DS1337RTC RTC = DS1337RTC(); // create an instance for the user
